<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('products')->insert([
            'title' => 'Black coffee',
            'image' => 'http://storecdn.keepcup.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/k/c/kc_brew_star-wars_rey__1_1.png',
            'price' => 1.20,
            'description' => 'Very tasty coffee'
        ]);

        DB::table('products')->insert([
            'title' => 'Coffee with milk',
            'image' => 'http://storecdn.keepcup.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/k/c/kc_longplay_star-wars_darth.png',
            'price' => 1.40,
            'description' => 'Very sweet coffee'
        ]);

        DB::table('products')->insert([
            'title' => 'Caffè mocha',
            'image' => 'http://storecdn.keepcup.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/d/a/darth-vader-12oz-original.jpg',
            'price' => 2.00,
            'description' => 'Chocolate-flavored coffee'
        ]);

        DB::table('products')->insert([
            'title' => 'Caffè Americano',
            'image' => 'http://storecdn.keepcup.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/s/t/stormtrooper-12oz-original.jpg',
            'price' => 1.80,
            'description' => 'Espresso with hot water'
        ]);
    }
}
